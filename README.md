Local installation instructions
---
1. Install [git](https://git-scm.com/downloads) and [XAMPP](https://apachefriends.org/download.html)
2. Start PHP and MySQL from XAMPP
3. Clone the repository
    1. Open `cmd`
    2. Change to the XAMPP directory with `cd C:\xampp\htdocs`
    3. Clone the repository with `git clone https://gitlab.com/Rocko97/Basic-POS.git`
3. Create the database
    1. Go to `localhost/phpmyadmin` in a web browser
    2. In phpMyAdmin, go to the "Databases" tab
    3. Enter `cis424` in the input field labeled "Create database" and click the "Create" button
4. Add the tables
    1. Click on the newly created database in the list databases
    2. Go to the "SQL" tab for that database
    3. Paste the code from the `source/tables.sql` file into the input area and click the "Go" button
5. Go to `localhost/Basic-POS` in a web browser to access the application

Style guide
---
1. Four spaces for indentation
2. Windows style line breaks, that is `CR` `LF`
3. Double quotes for strings in HTML and CSS
4. Single quotes for strings in PHP unless there is variable in the string
5. Omit ending slash in [void elements](https://html.spec.whatwg.org/multipage/syntax.html#void-elements), for example `<meta>` not `<meta/>`

