<!--
Add the PHP code you need.  This is just a page.
-->
<?php

require_once 'source/db.php';

$username = '';
$password = '';

$passwordsmatch = TRUE;
$userexists = TRUE;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['user'])) {
        $username = trim($_POST['user']);
    }

    if (isset($_POST['password'])) {
        $password = $_POST['password'];
    }

    if ($username && $password) {
        $user = db_get_user_by_user_name($username);

        if ($user) {
            $userexists = TRUE;
            $passwordsmatch = password_verify($password, $user['passwordhash']);

            if ($passwordsmatch) {
                session_start();
                $_SESSION['userid'] = $user['id'];
                $_SESSION['username'] = $user['username'];
                header('Location: dashboard');
            }
        }
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="styles/login.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>


<body>
<div class="container">
    <div class="row main">
        <div class="panel-heading">
            <div class="panel-title text-center">
                <h1 class="title">Basic POS</h1>
                <hr>
            </div>
        </div>
        <div class="main-login main-center">

            <?php if (!$userexists) { echo '<p>User does not exist for that username</p>'; } ?>
            <?php if (!$passwordsmatch) { echo '<p>Password does not match</p>'; } ?>
            <form class="form-horizontal" method="post" action="login">
                <div class="form-group">
                    <label for="user" class="cols-sm-2 control-label">User name</label>
                    <?php if ($user === '') { echo '<p>Password required</p>'; } ?>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="user" id="user" <?php if ($username !== '') { echo 'value="'.$username.'"'; } ?> placeholder="Enter your Name">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="password" class="cols-sm-2 control-label">Password</label>
                    <?php if (!$password === '') { echo '<p>Password required</p>'; } ?>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                            <input type="password" class="form-control" name="password" id="password" placeholder="Enter your Password">
                        </div>
                    </div>
                </div>

                <div class="form-group ">
                    <button type="submit" class="btn btn-primary btn-lg btn-block login-button">Sign In</button>
                </div>
                <div class="login-register">
                    <a href="register">Register</a>
                </div>
            </form>
        </div>
    </div>
</div>

</body>
</html>
