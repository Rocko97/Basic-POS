<?php
$title = 'Transactions';
$styles = array('transactions.css');
$scripts = array('transactions.js');

require_once 'source/header.php';
?>

<h3>Make transaction</h3>
<p id="transaction-message"></p>
<div class="transaction-main">
    <div class="transaction-left">
        <div class="transaction-table-container">
            <table class="transaction-table">
                <thead>
                    <tr>
                        <th class="transaction-number">Item Number</th>
                        <th class="transaction-description">Description</th>
                        <th class="transaction-quantity">Quantity</th>
                        <th class="transaction-price">Price</th>
                    </tr>
                </thead>
                <tbody id="transaction-tbody"></tbody>
            </table>
        </div>
        <div class="transaction-controls">
            <p>
              <label for="transaction-number-input">Item number</label>
              <input id="transaction-number-input" class="form-control" type="text">
              <button id="transaction-add-button" class="btn btn-primary" type="button">Add</button>
            </p>
            <p>
                <button id="transaction-cancel-button" class="btn btn-danger" type="button">Cancel</button>
                <button id="transaction-complete-button" class="btn btn-primary" type="button">Complete</button>
            </p>
            <dl class="transaction-total">
                <dt>Total</dt>
                <dd id="transaction-total">$0.00</dd>
            </dl>
        </div>
    </div>
    <div class="transaction-right">
        <p>
            <label for="transaction-quantity-input">Quantity</label>
            <input id="transaction-quantity-input" class="form-control item-input" type="number" min="1" step="0">
        </p>
        <p>
            <label for="transaction-price-input">Price</label>
            <input id="transaction-price-input" class="form-control item-input" type="number" min="0" max="99999999.99" step="0.01">
        </p>
        <p>
            <button id="transaction-remove-button" class="btn btn-danger" type="button">Remove Item</button>
        </p>
    </div>
</div>

<?php require_once 'source/footer.php'; ?>