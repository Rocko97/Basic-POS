<?php
$title = 'Receipts';
$styles = array('receipts.css');
$scripts = array('receipts.js');

require_once 'source/header.php';
?>

<h3>Receipts</h3>
<p class="inline-form">
    <label for="receipts-id-input">Transaction number</label>
    <input id="receipts-id-input" class="form-control" type="text">
    <button id="receipts-id-button" class="btn btn-primary" class="button">Submit</button>
</p>
<p id="receipt-messages"></p>
<div id="receipt-contents" hidden>
    <dl id="receipt-summary">
        <dt>Transaction number</dt>
        <dd id="receipt-id"></dd>
        <dt>User</dt>
        <dd id="receipt-user"></dd>
        <dt>Date</dt>
        <dd id="receipt-date"></dd>
        <dt>Total</dt>
        <dd id="receipt-total"></dd>
    </dl>
    <table id="receipt-table">
        <thead>
            <tr>
                <th class="receipt-number">Item number</th>
                <th class="receipt-description">Description</th>
                <th class="receipt-quantity">Quantity</th>
                <th class="receipt-price">Price</th>
            </tr>
        </thead>
        <tbody id="receipt-tbody"></tbody>
    </table>
</div>

<?php require_once 'source/footer.php'; ?>