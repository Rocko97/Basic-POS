<?php

require_once '../source/pos.php';
require_once '../source/db.php';

//verify user is logged in
if (!is_logged_in()) {
    exit(json_encode(array('error' => 'not logged in')));
}

//check the request is a GET request
if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
    exit(json_encode(array('error' => 'not GET request')));
}

//check a receipt id is supplied
if (!isset($_GET['id'])) {
    exit(json_encode(array('error' => 'missing receipt id')));
}

//receipt id must be an integer
if (!preg_match('/^\\d+$/', $_GET['id'])) {
    exit(json_encode(array('error' => 'invalid receipt id')));
}

//get the general information for the receipt
//who did the transaction, when it happened, and the total
$receiptsummary = db_get_receipt_summary($_GET['id']);

//verify receipt exists
if (!$receiptsummary) {
    exit(json_encode(array('error' => 'receipt not found')));
}

//get the specific items in the receipt
$receiptdetails = db_get_receipt_entries($_GET['id']);

//verify receipt exists
if (!$receiptdetails) {
    exit(json_encode(array('error' => 'receipt not found')));
}

//array for entries in the receipt
$entries = array();

//copy the entries to the array
foreach ($receiptdetails as $entry) {
    $entries[] = array('id' => $entry['id'],
                       'upc' => $entry['upc'],
                       'description' => $entry['description'],
                       'quantity' => $entry['quantity'],
                       'refunds' => $entry['refunds'],
                       'price' => $entry['price']);
}

//send the receipt back as JSON
echo json_encode(array('id' => $_GET['id'],
                       'user' => $receiptsummary['username'],
                       'date' => $receiptsummary['creationdate'],
                       'total' => $receiptsummary['total'],
                       'items' => $entries));