<?php

require_once '../source/pos.php';
require_once '../source/db.php';

//verify user is logged in
if (!is_logged_in()) {
    exit(json_encode(array('error' => 'not logged in')));
}

//get items from database
$items = db_get_all_items();

//verify items were received
if (!$items) {
    exit(json_encode(array('error' => 'database error')));
}

//array to store the items in
$result = array();

//loop though items from database adding them to the array
foreach ($items as $item) {
    $result[] = array('upc' => $item['upc'], 'description' => $item['description'], 'price' => $item['price']);
}

//send back the item details as in JSON format
echo json_encode($result);