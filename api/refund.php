<?php

/*
Form of request:
{
    "transaction": tranaction id number,
    "refunds": {
        "transaction entry id": refund quantity,
        "transaction entry id": refund quantity,
        "transaction entry id": refund quantity
    }
}
*/

require_once '../source/pos.php';
require_once '../source/db.php';

//verify user is logged in
if (!is_logged_in()) {
    exit(json_encode(array('error' => 'not logged in')));
}

//check the request is a POST request
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    exit(json_encode(array('error' => 'not POST request')));
}

//parse the request as JSON
$rawdata = file_get_contents('php://input');
$request = json_decode($rawdata, TRUE);

//make sure JSON parsed
if (!$request) {
    exit(json_encode(array('error' => 'invalid JSON')));
}

//make sure properties were set
if (!isset($request['transaction'], $request['refunds'])) {
    exit(json_encode(array('error' => 'missing properties in JSON')));
}

//the refunds variable from the JSON request is an associative array with
//the key an transaction entry id and the vlaue the number of items to refund
$refunds = $request['refunds'];

//verify the refunds are an associative array
if (!is_array($refunds)) {
    exit(json_encode(array('error' => 'invalid refunds property')));
}

//look up the transaction in the database
$entries = db_get_receipt_entries($request['transaction']);

//verify the transaction was found
if (!$entries) {
    exit(json_encode(array('error' => 'invalid transaction id')));
}

//add a refund to the database and get the id for that refund
$refundid = db_add_refund($_SESSION['userid'], $request['transaction']);

//make sure refund was added to the database
if (!$refundid) {
    exit(json_encode(array('error' => 'database error adding refund')));
}

//total variable to keep track of how much is refunded
$refundedtotal = 0;

//loop through all the entries in the tranaction checking if entries should be refunded
foreach ($entries as $entry) {
    //get properties stored in the database record for the transaction entry
    $price = $entry['price'];
    $transactionentryid = $entry['id'];
    $transactionquantity = $entry['quantity'];

    //see if the entry was refunded
    if (isset($refunds[$transactionentryid])) {
        //get how many items to refund
        $refundquantity = $refunds[$transactionentryid];

        //the number of items to refund cannot be more than the number of items purchased
        if ($refundquantity < 1 || $refundquantity > $transactionquantity) {
            //remove the refund from the database if the quantity was invalid
            db_delete_refund($refundid);
            exit(json_encode(array('error' => 'invalid quantity')));
        }

        //test if the refund entry was added to the database
        if (!db_add_refund_entry($refundid, $transactionentryid, $refundquantity)) {
            //remove the refund from the database if there was an error
            db_delete_refund($refundid);
            exit(json_encode(array('error' => 'database error adding entry')));
        }

        //multiple price by 100 to add integer cents instead of decimal dollars
        $refundedtotal += 100 * $price * $refundquantity;
    }
}

//make sure some amount was refunded
if ($refundedtotal === 0) {
    //remove the refund from the database if nothing was refunded
    db_delete_refund($refundid);
    exit(json_encode(array('error' => 'no items to refund')));
}

//convert the total back to decimal dollars
$refundedtotal = round($refundedtotal / 100, 2);

//send back the amount refunded in JSON format
echo json_encode(array('refund' => $refundedtotal));