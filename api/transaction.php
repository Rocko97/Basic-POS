<?php

require_once '../source/pos.php';
require_once '../source/db.php';

//verify user is logged in
if (!is_logged_in()) {
    exit(json_encode(array('error' => 'not logged in')));
}

//check the request is a POST request
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    exit(json_encode(array('error' => 'not POST request')));
}

//parse the reqeust as JSON
$rawdata = file_get_contents('php://input');
$items = json_decode($rawdata, TRUE);

//make sure JSON parsed
if (!$items) {
    exit(json_encode(array('error' => 'invalid JSON')));
}

//add a transaction to the database
$transactionid = db_add_transaction($_SESSION['userid']);

//make sure the transaction could be created
if (!$transactionid) {
    exit(json_encode(array('error' => 'database error')));
}

//array to record status for each item
$errors = array();

//a flag to determine whether there were any errors in the entires
$error = FALSE;

//make an entry for each item in the transcation
foreach ($items as $item) {
    //check that properties are set
    if (!isset($item['upc'], $item['quantity'], $item['price'])) {
        $errors[] = 'missing property';
        $error = TRUE;
        continue;
    }

    //UPC must be 1 to 12 digits
    if (!preg_match('/^\\d{1,12}$/', $item['upc'])) {
        $errors[] = 'invalid UPC';
        $error = TRUE;
        continue;
    }

    //quantity must be a positive integer
    if (!preg_match('/^[1-9][0-9]*$/', $item['quantity'])) {
        $errors[] = 'invalid quantity';
        $error = TRUE;
        continue;
    }

    //price must be an integer or a number with one or two deciaml places
    if (!preg_match('/(^\\d{1,8}$)|(^\\d{0,8}\\.\\d{1,2}$)/', $item['price'])) {
        $errors[] = 'invalid price';
        $error = TRUE;
        continue;
    }

    //find item with the UPC
    $itemdetail = db_get_item_by_upc($item['upc']);

    //verify the item exists
    if (!$itemdetail) {
        $errors[] = 'item does not exists';
        $error = TRUE;
        continue;
    }

    //add entry for the item to the database
    $result = db_add_transaction_entry($transactionid, $itemdetail['id'], $item['price'], $item['quantity']);

    //check that it was added successfully
    if (!$result) {
        $errors[] = 'database error';
        $error = TRUE;
        continue;
    }

    //otherwise use an empty string to represent no error
    $errors[] = '';
}

//remove the entire transaction from the database if any errors occurred
if ($error) {
    db_delete_transaction($transactionid);

    //send back JSON formatted array with message for each item entry
    //if there were no error for an item, the entry will be "item recorded"
    exit(json_encode($errors));
}

//if no error occurs, send back the transaction id
echo json_encode(array('id' => $transactionid));