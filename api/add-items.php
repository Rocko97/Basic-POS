<?php

require_once '../source/pos.php';
require_once '../source/db.php';

//verify user is logged in
if (!is_logged_in()) {
    exit(json_encode(array('error' => 'not logged in')));
}

//check the request is a POST request
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    exit(json_encode(array('error' => 'not POST request')));
}

//parse the reqeust as JSON
$rawdata = file_get_contents('php://input');
$items = json_decode($rawdata, TRUE);

//make sure JSON parsed
if (!$items) {
    exit(json_encode(array('error' => 'invalid JSON')));
}

//array to store whether each item was successfully added
$results = array();

//add each item
foreach ($items as $item) {
    //check that properties are set
    if (!isset($item['upc'], $item['description'], $item['price'])) {
        $results[] = 'missing property';
        continue;
    }

    //UPC must be 1 to 12 digits
    if (!preg_match('/^\\d{1,12}$/', $item['upc'])) {
        $results[] = 'invalid UPC';
        continue;
    }

    //description can be letters, numbers, spaces, or hyphens
    if (!preg_match('/^[a-zA-Z0-9 \\-]{1,100}$/', $item['description'])) {
        $results[] = 'invalid description';
        continue;
    }

    //price must be an integer or a number with one or two deciaml places
    if (!preg_match('/(^\\d{1,8}$)|(^\\d{0,8}\\.\\d{1,2}$)/', $item['price'])) {
        $results[] = 'invalid price';
        continue;
    }

    //check if item with the UPC already exists
    if (db_get_item_by_upc($item['upc'])) {
        $results[] = 'item already exists';
        continue;
    }

    //add the item to the database
    //the description is not escaped because the regular expression checks only all valid characters
    if (!db_add_item($item['upc'], $item['description'], $item['price'])) {
        $results[] = 'item not added';
        continue;
    }

    //otherwise the item was added successfully
    $results[] = 'item added';
}

//send back a JSON array detailing each item
echo json_encode($results);