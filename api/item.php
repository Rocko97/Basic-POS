<?php

require_once '../source/pos.php';
require_once '../source/db.php';

//verify user is logged in
if (!is_logged_in()) {
    exit(json_encode(array('error' => 'not logged in')));
}

//check the request is a GET request
if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
    exit(json_encode(array('error' => 'not GET request')));
}

//check a UPC is supplied
if (!isset($_GET['upc'])) {
    exit(json_encode(array('error' => 'missing UPC')));
}

//UPC must be 1 to 12 digits
if (!preg_match('/^\\d{1,12}$/', $_GET['upc'])) {
    exit(json_encode(array('error' => 'invalid UPC')));
}

//find item in database
$item = db_get_item_by_upc($_GET['upc']);

//check whether item was found
if (!$item) {
    exit(json_encode(array('error' => 'item not found')));
}

//send back the item details as in JSON format
echo json_encode(array('upc' => $item['upc'], 'description' => $item['description'], 'price' => $item['price']));