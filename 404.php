<!--
Add the PHP code you need.  This is just a page.
-->
<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="styles/login.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>


<body>
<div class="container">
    <div class="row main">
        <div class="panel-heading">
            <div class="panel-title text-center">
                <h1 class="title">Basic POS</h1>
                <hr />
            </div>
        </div>
        <div class="main-login main-center">

            <form class="form-horizontal" method="post" action="#">
                <label for="name" class="cols-sm-1 title-label" style="font-size: 20px;">Oops, looks like you found a page that is still under development!</label>


                <div class="login-register">
                    <a href="login.php">Back to Login</a>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript" src="assets/js/bootstrap.js"></script>
</body>
</html>
