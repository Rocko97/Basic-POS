<?php
$title = 'Refunds';
$styles = array('refunds.css');
$scripts = array('refunds.js');

require_once 'source/header.php';
?>

<h3>Refunds</h3>
<p class="inline-form">
    <label for="refunds-id-input">Transaction number</label>
    <input id="refunds-id-input" class="form-control" type="text">
    <button id="refunds-id-button" class="btn btn-primary" class="button">Submit</button>
</p>
<div id="refunds-contents" hidden>
    <dl id="refunds-summary">
        <dt>Transaction number</dt>
        <dd id="refunds-id"></dd>
        <dt>User</dt>
        <dd id="refunds-user"></dd>
        <dt>Date</dt>
        <dd id="refunds-date"></dd>
        <dt>Total</dt>
        <dd id="refunds-total"></dd>
    </dl>
    <table id="refunds-table">
        <thead>
            <tr>
                <th class="refunds-number">Item number</th>
                <th class="refunds-description">Description</th>
                <th class="refunds-quantity">Quantity</th>
                <th class="refunds-price">Price</th>
                <th class="refunds-price">Refunded</th>
            </tr>
        </thead>
        <tbody id="refunds-tbody"></tbody>
    </table>
    <div class="refunds-controls">
        <p class="form-inline">
            <label for="refunds-quantity-input">Number of items to refund</label>
            <input id="refunds-quantity-input" class="form-control" type="number" min="0" step="1">
        </p>
        <p>
            <button id="refunds-refund-button" class="btn btn-primary" class="button">Complete refund</button>
        </p>
        <dl class="refunds-amount">
            <dt>Amount refunded</dt>
            <dd id="refunds-amount">$0.00</dd>
        </dl>
    </div>
</div>
<p id="refunds-messages"></p>

<?php require_once 'source/footer.php'; ?>