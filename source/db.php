<?php

$host = 'localhost';
$dbname = 'cis424';
$user = 'root';
$password = '';

$db = new PDO("mysql:host=$host;dbname=$dbname", $user, $password);

function db_get_user_by_user_name($username)
{
    global $db;
    $sth = $db->prepare('SELECT id, firstname, lastname, username, passwordhash FROM users WHERE username=?');
    $sth->execute(array($username));
    return $sth->fetch();
}

function db_add_user($firstname, $lastname, $username, $passwordhash)
{
    global $db;
    $sth = $db->prepare('INSERT INTO users (firstname, lastname, username, passwordhash, creationdate) VALUES (?, ?, ?, ?, NOW())');
    return $sth->execute(array($firstname, $lastname, $username, $passwordhash));
}

function db_get_item_by_upc($upc)
{
    global $db;
    $sth = $db->prepare('SELECT id, upc, description, price FROM items WHERE upc=?');
    $sth->execute(array($upc));
    return $sth->fetch();
}

function db_add_item($upc, $description, $price)
{
    global $db;
    $sth = $db->prepare('INSERT INTO items (upc, description, price) VALUES (?, ?, ?)');
    return $sth->execute(array($upc, $description, $price));
}

function db_get_all_items()
{
    global $db;
    return $db->query('SELECT upc, description, price FROM items');
}

function db_add_transaction($userid)
{
    global $db;
    $sth = $db->prepare('INSERT INTO transactions (userid, creationdate) VALUES (?, NOW())');
    if ($sth->execute(array($userid))) {
        return $db->lastInsertId();
    } else {
        return FALSE;
    }
}

function db_add_transaction_entry($transactionid, $itemid, $price, $quantity)
{
    global $db;
    $sth = $db->prepare('INSERT INTO transactionentries (transactionid, itemid, price, quantity) VALUES (?, ?, ?, ?)');
    return $sth->execute(array($transactionid, $itemid, $price, $quantity));
}

function db_delete_transaction($transactionid)
{
    global $db;
    $sth = $db->prepare('DELETE FROM transactionentries WHERE transactionid=?');
    if ($sth->execute(array($transactionid))) {
        $sth = $db->prepare('DELETE FROM transactions WHERE id=?');
        return $sth->execute(array($transactionid));
    } else {
        return FALSE;
    }
}

function db_get_receipt_summary($transactionid)
{
    global $db;

    $sql = 'SELECT t.id, u.username, t.creationdate, 
                (SELECT SUM(te.price * (te.quantity - 
                    (SELECT COALESCE(SUM(re.quantity), 0)
                     FROM refundentries re
                     WHERE re.transactionentryid=te.id)))
                 FROM transactionentries te
                 WHERE te.transactionid=t.id) as total
            FROM transactions t
            JOIN users u ON u.id=t.userid
            WHERE t.id=?';

    $sth = $db->prepare($sql);
    $sth->execute(array($transactionid));
    return $sth->fetch();
}

function db_get_receipt_entries($transactionid)
{
    global $db;

    $sql = 'SELECT te.id, i.upc, i.description, te.price, te.quantity, SUM(COALESCE(re.quantity, 0)) as refunds
            FROM transactionentries te
            LEFT JOIN refundentries re ON re.transactionentryid=te.id
            JOIN items i ON i.id=te.itemid
            WHERE te.transactionid=?
            GROUP BY te.id, i.upc, i.description, te.price, te.quantity
            ORDER BY te.id ASC';

    $sth = $db->prepare($sql);
    $sth->execute(array($transactionid));
    return $sth->fetchAll();
}

function db_add_refund($userid, $transactionid)
{
    global $db;
    $sth = $db->prepare('INSERT INTO refunds (userid, transactionid, creationdate) VALUES (?, ?, NOW())');
    if ($sth->execute(array($userid, $transactionid))) {
        return $db->lastInsertId();
    } else {
        return FALSE;
    }
}

function db_add_refund_entry($refundid, $transactionentryid, $quantity)
{
    global $db;
    $sth = $db->prepare('INSERT INTO refundentries (refundid, transactionentryid, quantity) VALUES (?, ?, ?)');
    return $sth->execute(array($refundid, $transactionentryid, $quantity));
}

function db_get_refund_entries($transactionid)
{
    global $db;

    $sql = 'SELECT re.transactionentryid, SUM(re.quantity)
            FROM refunds r
            JOIN refundentries re ON re.refundid=r.id
            WHERE r.transactionid=?
            GROUP BY re.transactionentryid';

    $sth = $db->prepare($sql);
    $sth->execute(array($transactionid));
    return $sth->fetchAll();
}

function db_delete_refund($refundsid)
{
    global $db;
    $sth = $db->prepare('DELETE FROM refundentries WHERE refundsid=?');
    if ($sth->execute(array($refundsid))) {
        $sth = $db->prepare('DELETE FROM refunds WHERE id=?');
        return $sth->execute(array($refundsid));
    } else {
        return FALSE;
    }
}