DROP TABLE IF EXISTS transactionentries;
DROP TABLE IF EXISTS transactions;
DROP TABLE IF EXISTS refunds;
DROP TABLE IF EXISTS refundentries;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS items;

CREATE TABLE users (
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    firstname VARCHAR(255) NOT NULL,
    lastname VARCHAR(255) NOT NULL,
    username VARCHAR(255) UNIQUE NOT NULL,
    passwordhash CHAR(60) NOT NULL,
    creationdate DATETIME NOT NULL
) CHARACTER SET utf8 COLLATE utf8_unicode_ci;

CREATE TABLE items (
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    upc CHAR(12) UNIQUE NOT NULL,
    description VARCHAR(255) NOT NULL,
    price DECIMAL(10, 2) NOT NULL,
    creationdate DATETIME NOT NULL
) CHARACTER SET utf8 COLLATE utf8_unicode_ci;

CREATE TABLE transactions (
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    userid INT NOT NULL,
    creationdate DATETIME NOT NULL,
    FOREIGN KEY (userid) REFERENCES users(id)
) CHARACTER SET utf8 COLLATE utf8_unicode_ci;

CREATE TABLE transactionentries (
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    transactionid INT NOT NULL,
    itemid INT NOT NULL,
    price DECIMAL(10, 2) NOT NULL,
    quantity INT NOT NULL,
    FOREIGN KEY (transactionid) REFERENCES transactions(id),
    FOREIGN KEY (itemid) REFERENCES items(id)
) CHARACTER SET utf8 COLLATE utf8_unicode_ci;

CREATE TABLE refunds (
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    userid INT NOT NULL,
    transactionid INT NOT NULL,
    creationdate DATETIME NOT NULL,
    FOREIGN KEY (userid) REFERENCES users(id),
    FOREIGN KEY (transactionid) REFERENCES transactions(id)
) CHARACTER SET utf8 COLLATE utf8_unicode_ci;

CREATE TABLE refundentries (
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    refundid INT NOT NULL,
    transactionentryid INT NOT NULL,
    quantity INT NOT NULL,
    FOREIGN KEY (refundid) REFERENCES refunds(id),
    FOREIGN KEY (transactionentryid) REFERENCES transactionentries(id)
) CHARACTER SET utf8 COLLATE utf8_unicode_ci;
