<?php

require_once 'source/pos.php';

if (!is_logged_in()) {
    header('Location: login');
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <!-- Main styles for this application-->
    <link href="styles/main.css" rel="stylesheet">
    <?php
    if (isset($styles)) {
        foreach ($styles as $style) {
            echo '<link href="styles/'.$style.'" rel="stylesheet">'.PHP_EOL;
        }
    }
    ?>
    <title><?php if (isset($title)) {echo $title; } ?></title>
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
<header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#">
        <h3>Basic POS</h3>
    </a>
    <ul class="nav navbar-nav d-md-down-none px-4">
        <li class="nav-item px-3">
            <a class="nav-link" href="dashboard.php">Dashboard</a>
        </li>
        <li class="nav-item px-3">
            <a class="nav-link" href="404.php">Users</a>
        </li>
        <li class="nav-item px-3">
            <a class="nav-link" href="404.php">Settings</a>
        </li>
    </ul>
    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item d-md-down-none">
            <a class="nav-link" href="#">
                <i class="icon-list"></i>
            </a>
        </li>
        <li class="nav-item d-md-down-none">
            <a class="nav-link" href="#">
                <i class="icon-location-pin"></i>
            </a>
        </li>
        <li class="nav-item dropdown">
            Logged in as <?php echo $_SESSION['username']; ?>, <a href="logout">logout</a>

        </li>
    </ul>
    <button class="navbar-toggler aside-menu-toggler d-md-down-none" type="button" data-toggle="aside-menu-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <button class="navbar-toggler aside-menu-toggler d-lg-none" type="button" data-toggle="aside-menu-show">
        <span class="navbar-toggler-icon"></span>
    </button>
</header>
<div class="app-body">
    <div class="sidebar">
        <nav class="sidebar-nav">
            <ul class="nav">
                <li class="nav-title">Menu</li>
                <li class="nav-item">
                    <a class="nav-link" href="dashboard"><i class="nav-icon icon-speedometer"></i>Home</a>
                    <a class="nav-link" href="items"><i class="nav-icon icon-speedometer"></i>Items</a>
                    <a class="nav-link" href="transactions"><i class="nav-icon icon-speedometer"></i>Transactions</a>
                    <a class="nav-link" href="receipts"><i class="nav-icon icon-speedometer"></i>Receipts</a>
                    <a class="nav-link" href="refunds"><i class="nav-icon icon-speedometer"></i>Refunds</a>
                </li>
            </ul>
        </nav>
        <button class="sidebar-minimizer brand-minimizer" type="button"></button>
    </div>
    <main class="main">
        <!-- Breadcrumb-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item active">POS</li>
            <li class="breadcrumb-item active"><?php if (isset($title)) {echo $title; } ?></li>
            <!-- Breadcrumb Menu-->
            <li class="breadcrumb-menu d-md-down-none">
                <div class="btn-group" role="group" aria-label="Button group">
                    <a class="btn" href="#">
                        <i class="icon-speech"></i>
                    </a>
                    <a class="btn" href="404.php">
                        <i class="icon-graph"></i>  Dashboard</a>
                    <a class="btn" href="404.php">
                        <i class="icon-settings"></i>  Settings</a>
                </div>
            </li>
        </ol>
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">