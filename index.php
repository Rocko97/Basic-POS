<!DOCTYPE html>
<html>
    <head>
         <meta charset="utf-8">
         <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <link rel="stylesheet" href="styles/index.css">

         <title>Index</title>
    </head>
    <body>
        <h2>Basic POS System Index</h2>
        <table>
            <caption><h4>Creators</h4></caption>
            <tr>
                <th>Name</th>
                <th>Description</th>
            </tr>
            <tr>
                <td>Ashley List</td>
                <td>Product Owner</td>
            </tr>
            <tr>
                <td>Smith Kemper</td>
                <td>Scrum Master</td>
            </tr>
            <tr>
                <td>Garrett Lambert</td>
                <td>Development</td>
            </tr>
            <tr>
                <td>Jonathon Bates</td>
                <td>Development</td>
            </tr>
            <tr>
                <td>Chris Bordewyk</td>
                <td>Development</td>
            </tr>
            <article>This point-of-sale web application is for the use in both physical and online retail
                stores. It is intended to solve two primary problems with existing point-of-sale systems:
                interoperability with various devices and integration with ecommerce capabilities. By designing
                the system as a web application, merchants will be able to use the system on nearly any device
                that has a web browser; moreover, by developing a single application for both sales conducted in
                person and online, the system removes incompatibility from having separate systems for in store
                and online transactions. To login or register, click on the links below. </article>
        </table>






        <?php
        require_once 'source/pos.php';

        if (!is_logged_in()) {
            echo '<p><h4><a href="login">log in</a> or <a href="register">register</a> to go to the <a href="dashboard">dashboard</a></h4><p>';
        } else {
            echo 'hi '.$_SESSION['username'].', <a href="logout">Logout</a>';
        }

        ?>
    </body>
</html>