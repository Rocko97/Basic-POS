<!DOCTYPE html>
<html>

<head>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'>

    <title>Inventory</title>
</head>

<body>
<div class='container'>

    <main>
        <div class= 'dress 1'>
            <img src='Blue Dress.jpg' />
            <h2>Blue Dress</h2>
            <p class = 'price'>$20</p>
            <p class= 'description'>This blue dress is a classic, must have for every closet</p>
        </div>

        <div class='dress 2'>
            <img src='Grey Dress.jpg' />
            <h2>Grey Dress</h2>
            <p class = 'price'>$20</p>
            <p class = 'description'>Beaufitul grey dress, perfect for work</p>
        </div>

        <div class='combo 1'>
            <img src='Outfit Combo 1.jpg' />
            <h2>Outfit Combo Flannel</h2>
            <p class = 'price'>$45</p>
            <p class = 'description'>This fun combo comes with everything pictured here, including the bracelet and shoes, for one low price</p>
        </div>

        <div class='combo 2'>
            <img src='Outfit Combo 2.jpg' />
            <h2>Outfit Combo Double Hoodie</h2>
            <p class = 'price'>$45</p>
            <p class = 'description'>This fun combo comes with everything pictured here, including the sunglasses and shoes, for one low price</p>
        </div>

        <div class='daily jeans'>
            <img src='Daily Jeans.jpg' />
            <h2>Daily Jeans</h2>
            <p class = 'price'>$30</p>
            <p class = 'description'>The name says it all! These jeans are the must have and are perfect for any outfit combination</p>
        </div>

        <div class='ripped jeans'>
            <img src ='Ripped Jeans.jpg'/>
            <h2>Ripped Jeans</h2>
            <p class = 'price'>$30</p>
            <p class = 'description'>These are perfect to add a little spice to your outfit</p>
        </div>

        <div class='tequila shirt'>
            <img src= 'Tequila Shirt.jpg' />
            <h2>Tank Top</h2>
            <p class = 'price'>$20</p>
            <p class = 'description'>This fun tank will definitely liven up any party</p>
        </div>

        <div class='party shirt'>
            <img src='Party Shirt.jpg' />
            <h2>T-shirt Top</h2>
            <p class = 'price'>$20</p>
            <p class = 'description'>There ain't not party like a pizza party</p>
        </div>
    </main>
</div>
</body>
</html>