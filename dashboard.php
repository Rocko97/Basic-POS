<?php
$title = 'Dashboard';

require_once 'source/header.php';
?>

<h2>Point of sale application</h2>
<p>Final Project for CIS 424 Software Developemnt With Agile Methodology</p>
<p>By Ashley List, Christopher Bordewyk, Garrett Lambert, Jonathan Bates, and Smith Kemper<p>
<p>Source code available on <a href="https://gitlab.com/Rocko97/Basic-POS">GitLab</a></p>

<?php require_once 'source/footer.php'; ?>