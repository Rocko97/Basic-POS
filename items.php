<?php
$title = 'Items';
$styles = array('items.css');
$scripts = array('items.js');

require_once 'source/header.php';
?>

<div class="items-main">
    <div>
        <h3>Add item</h3>
        <p>
            <label for="add-number-input">Item number</label>
            <input id="add-number-input" class="form-control" type="text">
        </p>
        <p>
            <label for="add-description-input">Item description</label>
            <input id="add-description-input" class="form-control" type="text">
        </p>
        <p>
            <label for="add-price-input">Item price</label>
            <input id="add-price-input" class="form-control" type="number" min="0" max="99999999.99" step="0.01">
        </p>
        <p>
            <button id="add-button" class="btn btn-primary" type="button">Add</button>
        </p>
        <p id="add-messages"></p>
    </div>
    <div>
        <h3>Items list</h3>
        <table class="items-list-table">
            <thead>
                <tr>
                    <th class="items-list-number">Item number</th>
                    <th class="items-list-description">Description</th>
                    <th class="items-list-price">Price</th>
                </tr>
            </thead>
            <tbody id="items-list-tbody"></tbody>
        </table>
    </div>
    <div>
        <h3>Look up item</h3>
        <p>
            <label for="look-up-number-input">Item number</label>
            <input id="look-up-number-input" class="form-control" type="text">
        </p>
        <p>
            <button id="look-up-button" class="btn btn-primary" type="button">Look up</button>
        </p>
        <p id="look-up-messages" hidden></p>
        <dl id="look-up-item-details" hidden>
            <dt>Item number</dt>
            <dd id="look-up-number"></dd>
            <dt>Description</dt>
            <dd id="look-up-description"></dd>
            <dt>Price</dt>
            <dd id="look-up-price"></dd>
        </dl>
    </div>
</div>

<?php require_once 'source/footer.php'; ?>