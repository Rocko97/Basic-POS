<?php

//remove session cookie and redirect to main page
session_start();
unset($_SESSION['userid']);
unset($_SESSION['username']);
session_destroy();
header('Location: .');