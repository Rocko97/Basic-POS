$(function () {
    $('#receipts-id-input').keydown(function (event) {
        if (event.key === 'Enter') {
            getReceipt($('#receipts-id-input').val());
        }
    });
    
    $('#receipts-id-button').click(function () {
        getReceipt($('#receipts-id-input').val());
    });

    function getReceipt(receiptNumber) {
        $.getJSON('api/receipt/' + receiptNumber, function (response) {
            if (response.error) {
                $('#receipt-messages').fadeIn().text(response.error).delay(5000).fadeOut();
                $('#receipt-contents').hide();
            } else {
                $('#receipt-id').text(response.id);
                $('#receipt-user').text(response.user);
                $('#receipt-date').text(new Date(response.date).toLocaleString());
                $('#receipt-total').text('$' + response.total);

                $('#receipt-tbody').empty();
                $.each(response.items, function (i, item) {
                    $('#receipt-tbody').append('<tr class="receipt-row">'+
                        '<td class="receipt-number">' + item.upc + '</td>' +
                        '<td class="receipt-description">' + item.description + '</td>' + 
                        '<td class="receipt-quantity">' + (item.refunds>0?'<s>' + item.quantity + '</s> ' + (item.quantity - item.refunds):item.quantity) + '</td>' +
                        '<td class="receipt-price">$' + item.price + '</td></tr>');
                });
                $('#receipt-message').hide();
                $('#receipt-contents').show();
            }
        });
    }
});