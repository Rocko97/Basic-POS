$(function () {
    listItems();

    $('#add-button').click(function () {
        var upc = $('#add-number-input').val(),
            description = $('#add-description-input').val(),
            price = $('#add-price-input').val();

        addItem(upc, description, price);
    });

    $('#look-up-number-input').keydown(function (event) {
        if (event.key === 'Enter') {
            lookUpItem(this.value);
        }
    });

    $('#look-up-button').click(function () {
        lookUpItem($('#look-up-number-input').val());
    });
    
    function listItems() {
        $.getJSON('api/item-list', function (response) {
            $('#items-list-tbody').empty();
            $.each(response, function (i, item) {
                $('#items-list-tbody').append('<tr>' +
                    '<td>' + item.upc + '</td>' +
                    '<td>' + item.description + '</td>' +
                    '<td>$' + item.price + '</td></tr>');
            });
        })
    }

    function addItem(upc, description, price) {
        $.post('api/add-items', JSON.stringify([{
            upc: upc,
            description: description,
            price: price
        }]), function (response) {
            $('#add-messages').
                fadeIn().
                text(response.error || response.shift()).
                delay(5000).fadeOut();
            if (!response.error) {
                listItems();
            }
        }, 'json');
    }

    function lookUpItem(itemNumber) {
        $.getJSON('api/item/' + itemNumber, function (response) {
            if (response.error) {
                $('#look-up-item-details').hide();
                $('#look-up-messages').show().text(response.error).delay(5000).fadeOut();
            } else {
                $('#look-up-item-details').show();
                $('#look-up-messages').hide();
                $('#look-up-number').text(response.upc);
                $('#look-up-description').text(response.description);
                $('#look-up-price').text('$' + response.price);
            }
        });
    }
});
