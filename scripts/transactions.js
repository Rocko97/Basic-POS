$(function () {
    var entries = [],
        index = -1;

    $('#transaction-tbody').on('click', 'tr', function (event) {
        if ($(this).hasClass('selected')) {
            index = -1;
            $('.selected').removeClass('selected');
        } else {
            $('.selected').removeClass('selected');
            index = $(this).addClass('selected').index();
            $('#transaction-quantity-input').val(entries[index].quantity);
            $('#transaction-price-input').val(entries[index].price);
        }
    });

    $('#transaction-number-input').keydown(function (event) {
        if (event.key === 'Enter') {
            addEntry($('#transaction-number-input').val());
        }
    });

    $('#transaction-add-button').click(function () {
        addEntry($('#transaction-number-input').val());
    });

    $('#transaction-remove-button').click(function () {
        if (index > -1) {
            $('.selected').remove();
            entries.splice(index, 1);
            index = -1;
            updateTotal();
        }
    });

    $('#transaction-quantity-input').on('input', function () {
        if (index > -1) {
            entries[index].quantity = $(this).val();
            $('.transaction-quantity', $('.transaction-row')[index]).text(entries[index].quantity);
            updateTotal();
        }
    });

    $('#transaction-price-input').on('input', function () {
        if (index > -1) {
            entries[index].price = $(this).val();
            $('.transaction-price', $('.transaction-row')[index]).text('$' + (+entries[index].price).toFixed(2));
            updateTotal();
        }
    });

    $('#transaction-cancel-button').click(function () {
        $('#transaction-tbody').empty();
        entries = [];
        index = -1;
    });

    $('#transaction-complete-button').click(function () {
        $('.selected').removeClass('selected');
        $('.transaction-error').hide();
        index = -1;

        $.post('api/transaction', JSON.stringify(entries), function (response) {
            if (response.error) {
                $('#transaction-message').fadeIn().text(response.error).delay(4000).fadeOut();
            } else if (response.id) {
                $('#transaction-message').fadeIn().text('receipt id ' + response.id);
                $('#transaction-tbody').empty();
                index = -1;
                entries = [];
            } else {
                $('.transaction-row').each(function (i, row) {
                    $(this).append('<td class="transaction-error">' + response[i] + '</td>');
                });
            }
        }, 'json');
    });

    function updateTotal() {
        var total = 0;

        $.each(entries, function (i, entry) {
            total += entry.quantity * entry.price * 100;
        });
        
        total = (total / 100).toFixed(2);
        
        $('#transaction-total').text('$' + total);
    }
    
    function addEntry(itemNumber) {
        var matchingEntry = false;

        $.getJSON('api/item/' + itemNumber, function (response) {
            if (response.error) {
                $('#transaction-message').fadeIn().text(response.error).delay(4000).fadeOut();
            } else {
                $('.selected').removeClass('selected');

                $.each(entries, function (i, entry) {
                    if (entry.upc === response.upc && +entry.price === +response.price) {
                        index = i;
                        entry.quantity++;
                        $('.transaction-quantity', $('.transaction-row')[i]).text(entry.quantity);
                        matchingEntry = true;
                    }
                });
                
                if (!matchingEntry) {
                    index = entries.push({
                        upc: response.upc,
                        description: response.description,
                        quantity: 1,
                        price: response.price
                    }) - 1;

                    $('#transaction-tbody').append('<tr class="transaction-row">'+
                        '<td class="transaction-number">' + response.upc + '</td>' +
                        '<td class="transaction-description">' + response.description + '</td>' + 
                        '<td class="transaction-quantity">1</td>' +
                        '<td class="transaction-price">$' + response.price + '</td></tr>');
                }

                $('#transaction-quantity-input').val(entries[index].quantity);
                $('#transaction-price-input').val(entries[index].price);
                $($('.transaction-row')[index]).addClass('selected')[0].scrollIntoView();
                updateTotal();
                $('#transaction-number-input').val('');
            }
        });
    }
});