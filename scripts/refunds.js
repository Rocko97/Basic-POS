$(function () {
    var entries = [],
        transactionid = null,
        index = -1;

    $('#refunds-id-input').keydown(function (event) {
        if (event.key === 'Enter') {
            getRefund($('#refunds-id-input').val());
        }
    });
    
    $('#refunds-id-button').click(function () {
        getRefund($('#refunds-id-input').val());
    });

    $('#refunds-tbody').on('click', 'tr', function () {
        if ($(this).hasClass('.selected')) {
            $(this).removeClass('selected');
            index = -1;
        } else {
            $('.selected').removeClass('selected');
            index = $(this).addClass('selected').index();
            $('#refunds-quantity-input').val(entries[index].refunded).attr('max', entries[index].quantity - entries[index].refunds);
        }
    });

    $('#refunds-quantity-input').on('input', function () {
        var total = 0;

        if (index > -1) {
            entries[index].refunded = $('#refunds-quantity-input').val();
            $('.refunds-refunded', $('.refunds-row')[index]).text(entries[index].refunded);
            $.each(entries, function (i, entry) {
                if (+entry.refunded) {
                    total += entry.price * 100 * entry.refunded;
                }
            });

            $('#refunds-amount').text((total / 100).toFixed(2));
        }
    });

    $('#refunds-refund-button').click(function () {
        var refunds = {};

        $.each(entries, function (i, entry) {
            if (+entry.refunded) {
                refunds[entry.id] = entry.refunded;
            }
        });

        $.post('api/refund', JSON.stringify({
            transaction: transactionid,
            refunds: refunds
        }),function (response) {
            if (response.error) {
                $('#refunds-messages').fadeIn().text(response.error).delay(5000).fadeOut();
            } else {
                $('#refunds-messages').show().text('$' + response.refund + ' refunded');
                $('#refunds-contents').hide();
                $('#refunds-amount').text('$0.00');
                entries = [];
                index = -1;
            }
        }, 'json');
    });

    function getRefund(receiptNumber) {
        $.getJSON('api/receipt/' + receiptNumber, function (response) {
            if (response.error) {
                $('#refunds-messages').fadeIn().text(response.error).delay(5000).fadeOut();
                $('#refunds-contents').hide();
            } else {
                transactionid = receiptNumber;
                entries = response.items;

                $('#refunds-id').text(response.id);
                $('#refunds-user').text(response.user);
                $('#refunds-date').text(new Date(response.date).toLocaleString());
                $('#refunds-total').text('$' + response.total);

                $('#refunds-tbody').empty();
                $.each(response.items, function (i, item) {
                    $('#refunds-tbody').append('<tr class="refunds-row">'+
                        '<td class="refunds-number">' + item.upc + '</td>' +
                        '<td class="refunds-description">' + item.description + '</td>' + 
                        '<td class="refunds-quantity">' + (item.refunds>0?'<s>' + item.quantity + '</s> ' + (item.quantity - item.refunds):item.quantity) + '</td>' +
                        '<td class="refunds-price">$' + item.price + '</td>' +
                        '<td class="refunds-refunded"></td></tr>');
                });
                $('#refunds-messages').hide();
                $('#refunds-contents').show();
            }
        });
    }
});