<!--
Add the PHP code you need.  This is just a page.
-->
<?php

require_once 'source/db.php';

$user = '';
$last = '';
$first = '';
$password = '';
$confirm = '';

$firstvalid = TRUE;
$lastvalid = TRUE;
$uservalid = TRUE;
$passwordvalid = TRUE;
$confirmvalid = TRUE;
$alreadyregisterd = FALSE;
$success = TRUE;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['first'])) {
        $first = trim($_POST['first']);
        $firstvalid = preg_match('/^[a-z]+$/i', $first);
    }

    if (isset($_POST['last'])) {
        $last = trim($_POST['last']);
        $lastvalid = preg_match('/^[a-z]+$/i', $last);
    }

    if (isset($_POST['user'])) {
        $user = trim($_POST['user']);
        $uservalid = preg_match('/^[a-z]+$/i', $user);
    }

    if (isset($_POST['password'])) {
        $password = $_POST['password'];
        $passwordvalid = strlen($password) >= 3;
    }
    
    if (isset($_POST['confirm'])) {
        $confirm = $_POST['confirm'];
        $confirmvalid = $password === $confirm;
    }

    if ($firstvalid && $lastvalid && $uservalid & $passwordvalid && $confirmvalid) {
        $alreadyregisterd = db_get_user_by_user_name($user);
        
        if (!$alreadyregisterd) {
            $passwordhash = password_hash($password, PASSWORD_BCRYPT);
            db_add_user($first, $last, $user, $passwordhash);
            $userrecord = db_get_user_by_user_name($user);

            if ($userrecord) {
                session_start();
                $_SESSION['userid'] = $userrecord['id'];
                $_SESSION['username'] = $userrecord['username'];
                header('Location: dashboard');
            }
        }
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Register</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="styles/login.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>


<body>
<div class="container">
    <div class="row main">
        <div class="panel-heading">
            <div class="panel-title text-center">
                <h1 class="title">Basic POS</h1>
                <hr>
            </div>
        </div>
        <div class="main-login main-center">

            <form class="form-horizontal" method="post" action="register">
                <div>
                    <label class="title-label">Please Create an Account</label>
                </div>
                <?php if ($alreadyregisterd) { echo '<p>A user with user name is already registerd</p>'; } ?>
                <?php if (!$success) { echo '<p>Registration unnsucessful</p>'; } ?>
                <div class="form-group">
                    <label for="first" class="cols-sm-2 control-label">First name</label>
                    <?php if(!$firstvalid) { echo '<p>Invlaid first name</p>'; } ?>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="first" id="first" <?php if ($first !== '') { echo 'value="'.$first.'"'; } ?> placeholder="Enter your first name">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="last" class="cols-sm-2 control-label">Last name</label>
                    <?php if (!$lastvalid) { echo '<p>Invlaid last name</p>'; } ?>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="last" id="last" <?php if ($last !== '') { echo 'value="'.$last.'"'; } ?> placeholder="Enter your last name">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="user" class="cols-sm-2 control-label">User name</label>
                    <?php if (!$uservalid) { echo '<p>Invlaid user name</p>'; } ?>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="user" id="user" <?php if ($user !== '') { echo 'value="'.$user.'"'; } ?> placeholder="Enter your user name">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="password" class="cols-sm-2 control-label">Password</label>
                    <?php if (!$passwordvalid) { echo '<p>Password must be at least 3 characters</p>'; } ?>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                            <input type="password" class="form-control" name="password" id="password" <?php if ($password !== '') { echo 'value="'.$password.'"'; } ?> placeholder="Enter your Password">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="confirm" class="cols-sm-2 control-label">Confirm Password</label>
                    <?php if (!$confirmvalid) { echo '<p>Passwords do not match</p>'; } ?>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                            <input type="password" class="form-control" name="confirm" id="confirm" <?php if ($confirm !== '') { echo 'value="'.$confirm.'"'; } ?> placeholder="Confirm your Password">
                        </div>
                    </div>
                </div>

                <div class="form-group ">
                    <button type="submit" class="btn btn-primary btn-lg btn-block login-button">Register</button>
                </div>
                <div class="login-register">
                    <a href="login">Login</a>
                </div>
            </form>
        </div>
    </div>
</div>

</body>
</html>
